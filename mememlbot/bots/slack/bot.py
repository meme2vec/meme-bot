"""Code for MemeMLBot Slack Bot."""
import logging
import os

import slack

from common import base


class MemeMLSlackBot(base.MemeMLBot):
    def __init__(self):
        super().__init__()

    def run(self, token):
        rtm = slack.RTMClient(token=token)
        rtm.on(event='hello', callback=self.on_ready)
        rtm.on(event='message', callback=self.on_message)

        try:
            rtm.start()
        except KeyboardInterrupt:
            rtm.stop()

    def on_ready(self, **kwargs):
        # NOTE(pbielak): This method must be overridden, because Slack's RTM
        # client needs a function signature, that accept kwargs.
        super().on_ready()

    def on_message(self, **payload):
        data = payload["data"]
        web_client = payload["web_client"]

        if data.get('subtype') or data.get('upload'):
            return

        channel_id = data.get("channel")
        user_id = data.get("user")
        text = data.get("text")

        super().on_message(
            client=web_client,
            channel=channel_id,
            user=user_id,
            msg=text,
        )

    def send_message(self, client, channel, text, attachment=None):
        if attachment is not None:
            client.files_upload(
                channels=channel,
                initial_comment=text,
                file=attachment,
                filename='meme.png',
            )
        else:
            client.chat_postMessage(
                channel=channel,
                text=text
            )

    def _mention(self, user):
        return '<@%s>' % user


def main():
    logging.basicConfig(level=logging.INFO)
    bot = MemeMLSlackBot()
    bot.run(token=os.getenv('SLACK_API_TOKEN'))


if __name__ == '__main__':
    main()


"""Code for MemeMLBot Discord Bot."""
import logging
import os

import asyncio
import discord

from common import base


class MemeMLDiscordBot(base.MemeMLBot):
    def __init__(self):
        super().__init__()
        self._dc = discord.Client(
            activity=discord.Game(name='Preparing fresh memes'),
        )

        self._dc.on_ready = self.on_ready
        self._dc.on_message = self.receive_msg

    def run(self, token):
        self._dc.run(token)

    async def on_ready(self):
        super().on_ready()

    async def receive_msg(self, msg):
        content = msg.content
        author = msg.author
        channel = msg.channel

        if author == self._dc.user:
            return

        super().on_message(
            client=None,
            channel=channel,
            user=author,
            msg=content,
        )

    def send_message(self, client, channel, text, attachment=None):
        _loop = asyncio.get_event_loop()

        if attachment is None:
            asyncio.run_coroutine_threadsafe(channel.send(text), _loop)
        else:
            asyncio.run_coroutine_threadsafe(channel.send(
                text,
                file=discord.File(attachment, 'meme.png')
            ), _loop)

    def _mention(self, user):
        return user.mention


def main():
    logging.basicConfig(level=logging.INFO)
    client = MemeMLDiscordBot()
    client.run(os.getenv('DISCORD_TOKEN'))


if __name__ == '__main__':
    main()


"""Command parser for MemeMLBot."""
import re


class CommandParser:
    def __init__(self):
        self._prefix = None
        self._cmds = []
        self._default_clb = None

    def with_prefix(self, prefix):
        self._prefix = prefix
        return self

    def with_command(self, cmd_regex, callback, args=None):
        self._cmds.append({
            'regex': re.compile(cmd_regex),
            'clb': callback,
            'args': args or [],
        })
        return self

    def with_default(self, callback):
        self._default_clb = callback
        return self

    def parse(self, text, **kwargs):
        if not text.startswith(self._prefix):
            return

        text = text.split(self._prefix)[1].strip()

        for cmd in self._cmds:
            matches = re.match(cmd['regex'], text)
            if matches:
                kwargs.update(
                    dict(zip(cmd['args'], matches.groups()))
                )
                cmd['clb'](**kwargs)
                return

        self._default_clb(text=text, **kwargs)

    @property
    def prefix(self):
        return self._prefix

from pathlib import Path
import textwrap

from PIL import Image, ImageDraw, ImageFont


class ImageGenerator:
    def __init__(self, templates_dir, font_path):
        self.templates_dir = Path(templates_dir)
        self.font_path = font_path

    def generate_meme(self, template, top_text, bottom_text='',
                      font_size=9):
        img = Image.open(self.templates_dir / template)
        draw = ImageDraw.Draw(img)
        image_width, image_height = img.size

        font = ImageFont.truetype(font=self.font_path,
                                  size=int(image_height * font_size) // 100)

        top_text = top_text.upper()
        bottom_text = bottom_text.upper()

        bottom_lines, top_lines, char_height = self._wrap_lines(
            bottom_text, top_text, font, image_width
        )

        self._draw_lines(bottom_lines, top_lines, char_height, draw, font,
                         *img.size)
        return img

    def _draw_lines(self, bot_lines, top_lines,
                    char_height, draw, font, img_width, img_height):

        self._draw_top_lines(draw, font, img_width, top_lines)

        if bot_lines != '':
            self._draw_bottom_lines(bot_lines, char_height, draw, font,
                                    img_height, img_width)

    @staticmethod
    def _wrap_lines(bottom_text, top_text, font, image_width):
        char_width, char_height = font.getsize('A')
        chars_per_line = image_width // char_width
        top_lines = textwrap.wrap(top_text, width=chars_per_line)
        bottom_lines = textwrap.wrap(bottom_text, width=chars_per_line)
        return bottom_lines, top_lines, char_height

    @staticmethod
    def _draw_bottom_lines(lines, char_height, draw, font,
                           image_height, image_width):
        y = image_height - char_height * len(lines) - 15
        for line in lines:
            line_width, line_height = font.getsize(line)
            x = (image_width - line_width) / 2
            draw.text((x, y), line, fill='white', font=font)
            y += line_height

    @staticmethod
    def _draw_top_lines(draw, font, image_width, top_lines):
        y = 10
        for line in top_lines:
            line_width, line_height = font.getsize(line)
            x = (image_width - line_width) / 2
            draw.text((x, y), line, fill='white', font=font)
            y += line_height


flask==1.0.3
keras==2.2.4
numpy==1.16.3
pillow==6.0.0
spacy==2.1.4
tensorflow==1.13.1
